# Z step measurements

This is a Jupyter workbook to estimate the step size in z of an OpenFlexure Microscope. The procedure (adapted from [Grant _et al._](https://royalsocietypublishing.org/doi/10.1098/rsos.191921)) requires a stack of precision thickness coverslips with whiteboard pen on each, giving the microscope something to focus on.

The procedure moves the microscope's focal point from below the stack to above it, tracking the position and image sharpness. The sharpness will have a local peak at each coverslip interface. The distance in steps between each peak can be used with the knowledge of the given coverslip thickness to estimate the movement distance in z for a single step.

As this procedure requires a large depth of field, a lower magnification objective ~20× is recommended.
